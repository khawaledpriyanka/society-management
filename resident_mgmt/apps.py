from django.apps import AppConfig


class ResidentMgmtConfig(AppConfig):
    name = 'resident_mgmt'
