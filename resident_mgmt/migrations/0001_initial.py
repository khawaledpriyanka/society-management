# Generated by Django 2.2.3 on 2019-10-22 20:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('property', '0006_property_services'),
    ]

    operations = [
        migrations.CreateModel(
            name='Risident_info',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tenant_name', models.CharField(max_length=100, unique=True)),
                ('is_owner', models.BooleanField(default=True)),
                ('lease_from', models.DateField()),
                ('lease_to', models.DateField(null=True)),
                ('lease_renewal', models.DateField(null=True)),
                ('move_in', models.DateField()),
                ('move_out', models.DateField(null=True)),
                ('tenant_status', models.CharField(max_length=25)),
                ('roommates', models.IntegerField(null=True)),
                ('mobile', models.CharField(max_length=12, unique=True)),
                ('is_ref', models.BooleanField(default=False)),
                ('ref_name', models.CharField(max_length=100, null=True)),
                ('email', models.EmailField(max_length=254)),
                ('rent', models.FloatField(null=True)),
                ('address', models.TextField()),
                ('property_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='property.Property_Info')),
                ('unit_no', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='property.Unit')),
            ],
        ),
        migrations.CreateModel(
            name='ledger',
            fields=[
                ('tenant_acc', models.AutoField(primary_key=True, serialize=False)),
                ('tenant_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='resident_mgmt.Risident_info')),
            ],
        ),
    ]
