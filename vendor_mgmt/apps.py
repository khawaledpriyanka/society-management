from django.apps import AppConfig


class VendorMgmtConfig(AppConfig):
    name = 'vendor_mgmt'
